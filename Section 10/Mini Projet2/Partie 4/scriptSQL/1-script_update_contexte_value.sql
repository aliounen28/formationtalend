-- Insertion d'une nouvelle variable de contexte sendMail
INSERT INTO "PARAMS_LOG"."CONTEXTE"
VALUES ('sendMail', 'False');


-- Mise à jour la variable de contexte mailTo 
UPDATE "PARAMS_LOG"."CONTEXTE"
SET    value = 'mail du destinataire'
WHERE  key = 'mailTo';

-- Mise à jour la variable de contexte mailFrom 
UPDATE "PARAMS_LOG"."CONTEXTE"
SET    value = 'mail de l''expéditeur'
WHERE  key = 'mailFrom';

-- Mise à jour la variable de contexte mailSenderName 
UPDATE "PARAMS_LOG"."CONTEXTE"
SET    value = 'Nom de l''expéditeur'
WHERE  key = 'mailSenderName';

-- Mise à jour la variable de contexte mailUsername 
UPDATE "PARAMS_LOG"."CONTEXTE"
SET    value = 'mail de l''expéditeur'
WHERE  key = 'mailUsername';

-- Mise à jour la variable de contexte mailPassword 
UPDATE "PARAMS_LOG"."CONTEXTE"
SET    value = 'votreMotDePasse'
WHERE  key = 'mailPassword';

-- Mise à jour la variable de contexte sendMail 
UPDATE "PARAMS_LOG"."CONTEXTE"
SET    value = 'True'
WHERE  key = 'sendMail';